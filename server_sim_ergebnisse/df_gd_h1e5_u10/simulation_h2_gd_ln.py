# Libraries:
# System libraries
import random
import copy
# Custom libraries
import pandas as pd
import tensorflow as tf
import numpy as np

# Listen Objekte für DFs
nr_param = []
nr_input = []
dfs_forw = []
dfs_back = []
dfs_cent = []

for k in (range(1,10)):
  # Sim der Daten
  n = 500
  k = k
  X = np.random.uniform(low = 0.0, high = 1.0, size = n * k)
  X = X.reshape(n, k)
  koef = np.random.uniform(low = 1.0, high = 5.0, size = k)
  koef = koef.reshape(k,1)
  noise = np.random.normal(loc=0.0, scale=1.0, size=n)
  noise = noise.reshape(n, 1)
  y = 1 + np.matmul(X, koef) + noise
  y = pd.DataFrame(y)
  X = pd.DataFrame(X)
  h = 1e-5
  # Fitten des Grundmodells
  random.seed(42)
  tf.random.set_seed(42)
  opt = tf.keras.optimizers.SGD(
          learning_rate=0.01, momentum=0.0, nesterov=False, name='SGD'
        )
  tf.random.set_seed(42)
  model0 = tf.keras.Sequential()
  model0.add(tf.keras.layers.Dense(10, activation='linear', batch_input_shape=(None, k)))
  model0.add(tf.keras.layers.Dense(10, activation='linear'))
  model0.add(tf.keras.layers.Dense(1,  activation='linear'))
  model0.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
  hist0 = model0.fit(X, y, epochs = 750, batch_size=n, use_multiprocessing=True, verbose=0)
  gewichte0 = np.concatenate((model0.get_weights()[1], model0.get_weights()[0]), axis=None)
  yhat = model0.predict(X)
  trainableParams = np.sum([np.prod(v.get_shape()) for v in model0.trainable_weights])
  nonTrainableParams = np.sum([np.prod(v.get_shape()) for v in model0.non_trainable_weights])
  ## Forward Modelle
  y_iter = []
  gewichte_m = [ ]
  fits = []
  for i in (range(0,n)):
    temp_y = copy.deepcopy(y)
    temp_y.loc[i] += h
    y_iter.append(temp_y)
    random.seed(42)
    tf.random.set_seed(42)
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Dense(10,  activation='linear', batch_input_shape=(None, k)))
    model.add(tf.keras.layers.Dense(10,  activation='linear'))
    model.add(tf.keras.layers.Dense(1,  activation='linear'))
    model.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
    hist = model.fit(X, temp_y, epochs = 750, batch_size=n, use_multiprocessing=True, verbose=0)
    coefs_m = np.vstack((model.get_weights()[0]))
    gewichte_m.append( coefs_m )
    temp_pred = model.predict(X)
    fits.append(temp_pred[i])
  fits_f = copy.deepcopy(np.array(fits))
  ## Backward Modelle
  y_iter = []
  gewichte_m = [ ]
  fits = []
  for i in (range(0,n)):
    temp_y = copy.deepcopy(y)
    temp_y.loc[i] -= h
    y_iter.append(temp_y)
    random.seed(42)
    tf.random.set_seed(42)
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Dense(10,  activation='linear', batch_input_shape=(None, k)))
    model.add(tf.keras.layers.Dense(10,  activation='linear'))
    model.add(tf.keras.layers.Dense(1,  activation='linear'))
    model.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
    hist = model.fit(X, temp_y, epochs = 750, batch_size=n, use_multiprocessing=True, verbose=0)
    coefs_m = np.vstack((model.get_weights()[0]))
    gewichte_m.append( coefs_m )
    temp_pred = model.predict(X)
    fits.append(temp_pred[i])
  fits_b = copy.deepcopy(np.array(fits))
  # Berechnen der DFs
  forwdf = (np.sum((fits_f - yhat) / h))
  backdf = (np.sum((yhat - fits_b) / h))
  centdf = (np.sum((fits_f - fits_b) / (2 * h)))
  dfs_forw.append(forwdf)
  dfs_back.append(backdf)
  dfs_cent.append(centdf)
  nr_param.append(trainableParams)
  nr_input.append(k)

dfs = pd.DataFrame((nr_input, nr_param, dfs_forw, dfs_back, dfs_cent))

# Speichern der 
dfs.to_csv('dfs_forw_h2_gd_ln.csv', header=False, index=False)