# Libraries:
# System libraries
import random
import copy
from tqdm import tqdm  
# Custom libraries
import pandas as pd
import tensorflow as tf
import numpy as np

random.seed(42)
tf.random.set_seed(42)
opt = tf.keras.optimizers.SGD(
        learning_rate=0.1, momentum=0.0, nesterov=False, name='SGD'
        )
tf.random.set_seed(42)
moep0 = tf.keras.Sequential()

# Listen Objekte für DFs
nr_param = []
nr_input = []
dfs_forw = []
dfs_back = []
dfs_cent = []

for k in tqdm(range(1,2)):
    n = 100
    k = k
    p = k + 1
    X = np.random.uniform(low = 0.0, high = 1.0, size = n * k)
    X = X.reshape(n, k)
    x0 = np.ones(n)
    x0 = x0.reshape(n, 1)
    koef = np.random.uniform(low = 1.0, high = 5.0, size = k)
    koef = koef.reshape(k,1)
    noise = np.random.normal(loc=0.0, scale=.1, size=n)
    noise = noise.reshape(n, 1)
    y = 1 + np.matmul(X, koef) + noise
    y = pd.DataFrame(y)
    X = pd.DataFrame(X)
    x = np.hstack((x0, X))
    h = 1e-3
    # Fitten des Grundmodells
    random.seed(42)
    tf.random.set_seed(42)
    opt = tf.keras.optimizers.SGD(
            learning_rate=0.1, momentum=0.0, nesterov=False, name='SGD'
            )
    tf.random.set_seed(42)
    model0 = tf.keras.Sequential()
    model0.add(tf.keras.layers.Dense(1, use_bias = False, activation='linear', batch_input_shape=(None, p)))
    model0.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
    hist0 = model0.fit(x, y, epochs = 1000, batch_size=n, use_multiprocessing=True, verbose=0)
    yhat = model0.predict(x)
    trainableParams = np.sum([np.prod(v.get_shape()) for v in model0.trainable_weights])
    nonTrainableParams = np.sum([np.prod(v.get_shape()) for v in model0.non_trainable_weights])
    ## Forward Modelle
    y_iter = []
    gewichte_m = [ ]
    fits = []
    for i in (range(0,n)):
        temp_y = copy.deepcopy(y)
        temp_y.loc[i] += h
        y_iter.append(temp_y)
        random.seed(42)
        tf.random.set_seed(42)
        model = tf.keras.Sequential()
        model.add(tf.keras.layers.Dense(1,  activation='linear', batch_input_shape=(None, p)))
        model.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
        hist = model.fit(x, temp_y, epochs = 1000, batch_size=n, use_multiprocessing=True, verbose=0)
        coefs_m = np.vstack((model.get_weights()[0]))
        gewichte_m.append( coefs_m )
        temp_pred = model.predict(x)
        fits.append(temp_pred[i])
    fits_f = copy.deepcopy(np.array(fits))
    ## Backward Modelle
    y_iter = []
    gewichte_m = [ ]
    fits = []
    for i in (range(0,n)):
        temp_y = copy.deepcopy(y)
        temp_y.loc[i] -= h
        y_iter.append(temp_y)
        random.seed(42)
        tf.random.set_seed(42)
        model = tf.keras.Sequential()
        model.add(tf.keras.layers.Dense(1,  activation='linear', batch_input_shape=(None, p)))
        model.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
        hist = model.fit(x, temp_y, epochs = 1000, batch_size=n, use_multiprocessing=True, verbose=0)
        coefs_m = np.vstack((model.get_weights()[0]))
        gewichte_m.append( coefs_m )
        temp_pred = model.predict(x)
        fits.append(temp_pred[i])
    fits_b = copy.deepcopy(np.array(fits))
    # Berechnen der DFs
    forwdf = (np.sum((fits_f - yhat) / h))
    backdf = (np.sum((yhat - fits_b) / h))
    centdf = (np.sum((fits_f - fits_b) / (2 * h)))
    dfs_forw.append(forwdf)
    dfs_back.append(backdf)
    dfs_cent.append(centdf)
    nr_param.append(trainableParams)
    nr_input.append(k)


dfs = pd.DataFrame((nr_input, nr_param, dfs_forw, dfs_back, dfs_cent))
print(dfs)
# Speichern der 
dfs.to_csv('moep.csv', header=False, index=False)