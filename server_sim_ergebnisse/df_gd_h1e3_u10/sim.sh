#!/bin/bash
# erstellen einer py local env
# installiert alle dep
echo 'Was ist der Name der Dependencies Datei? (ohne .txt)'
read dep

echo 'Genutzte Python Version: '
python3 --version
# Erstellen des envs
python3 -m venv .simulation
echo 'Virtuelle Umgebung erfolgreich erschaffen.'
source .simulation/bin/activate

# installation der dependencies
pip3 install -r $dep.txt
echo 'Alle Dependencies erfolgreich installiert.'

# Eingabe der Python scripte 
# Ziel hier brutforce alle rein und parallel berechnen.
python3 simulation_h0_gd_ln.py & 
python3 simulation_h1_gd_ln.py & 
python3 simulation_h1_gd_relu.py &
python3 simulation_h2_gd_ln.py &
python3 simulation_h2_gd_relu.py &
python3 simulation_h3_gd_ln.py &
python3 simulation_h3_gd_relu.py &
wait

# Löschen des Environments
rm -r .simulation

# ende
echo 'Simulationen Erfolgreich abgeschlossen.'
