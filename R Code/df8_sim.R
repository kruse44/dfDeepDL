# Anschauen von y_hat = X beta_hat

# Libraries
# Model y = beta0 + residuals

# Daten Simulation:
n = 1000
residuals = rnorm(n = n, mean = 0, sd = 1)
betas = c(1, 2, 3, 4, 5, 6, 7, 8)
X = matrix(c(rep(1, times = n), runif(n  = n, min = 1, max = 2), runif(n  = n, min = 1, max = 2), runif(n  = n, min = 1, max = 2), runif(n  = n, min = 1, max = 2), runif(n  = n, min = 1, max = 2), runif(n  = n, min = 1, max = 2), runif(n  = n, min = 1, max = 2)), nrow = n)
# Xb :
Xb = X %*% betas
y = Xb + residuals

# Schritt 1: Basemodel fitten
baseline = lm(y~ 1 + X[, 2] + X[, 3] + X[, 4] + X[, 5] + X[, 6] + X[, 7] + X[, 8] )
summary(baseline)
# prediction
dataf = as.data.frame(X)
base_pred = predict.lm(object = baseline, newdata = dataf)
base_fit = baseline$fitted.values
# Schritt 2: auf i-te Zeile h addieren
# y + h und iterativ u. model fitten
h = 0.001

# h auf jedes y_i addieren
# dann model fitten und die fitted values vergleichn

# List aller gefitteten Modelle
model_list = list()

for (i in 1:n) {
  temp_y = y
  temp_X = X
  temp_y[i] = temp_y[i] + h
  temp_m = lm(temp_y ~ 1 + X[, 2] + X[, 3] + X[, 4] + X[, 5] + X[, 6] + X[, 7] + X[, 8] )
  model_list[[i]] = temp_m
}

sum((model_list[[1]]$fitted.values - baseline$fitted.values)/h)

# Differencen ausrechnen

# Liste aller differenzen
model_pred = list()
for (i in 1:n) {
  temp_v = sum((model_list[[i]]$fitted.values[[i]] - baseline$fitted.values[[i]])/h)
  model_pred[[i]] = temp_v
  print(temp_v)
}

sum(unlist(model_pred))
