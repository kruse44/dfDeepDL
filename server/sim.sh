#!/bin/bash
# Erstellt ein python local env 
# installiert alle dependencies

echo 'Wie heißt die Dependencies Datei? (ohne .txt):'
read dep

# Automatische Erstellung des localen Venvis:
echo 'Genutze Python Version:'
python3 --version
python3 -m venv .$simulation       # erstellt locales venv
echo 'Virtuelle Umgebung geschaffen!'
echo 'Aktiviere Umgebung.'
source .$simulation/bin/activate
echo 'Umgebung aktiviert!'

# Benutzer Eingabe der Dependencies:
pip3 install -r $dep.txt
echo 'Alle Dependencies installiert!'

# Benutzer Eingabe der Quelldatei
# Ausgabe des Zeitpunktes wo die Berechnung durch ist:
echo 'Start der Simulation.'
python3 simulation_h0_gd_ln.py &
python3 simulation_h1_gd_ln.py &
python3 simulation_h1_gd_relu.py &
python3 simulation_h2_gd_ln.py &
python3 simulation_h2_gd_relu.py &
python3 simulation_h3_gd_ln.py &
python3 simulation_h3_gd_relu.py &


# Löschen des .venv
rm -r .$simulation
echo 'Umgebung gelöscht!'

# Fertig!
echo 'Berechnungen beendet!'
