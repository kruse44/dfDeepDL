#!/bin/bash
# Erstellt ein python local env 
# installiert alle dependencies

echo 'Wie heißt die Dependencies Datei? (ohne .txt):'
read dep
# Benutzer Eingabe der Quelldatei
echo 'Welche Datei soll ausgeführt werden? (ohne .py):'
read simulation


# Automatische Erstellung des localen Venvis:
echo 'Genutze Python Version:'
python3 --version
python3 -m venv .$simulation       # erstellt locales venv
echo 'Virtuelle Umgebung geschaffen!'
echo 'Aktiviere Umgebung.'
source .$simulation/bin/activate
echo 'Umgebung aktiviert!'

# Benutzer Eingabe der Dependencies:
pip3 install -r $dep.txt
echo 'Alle Dependencies installiert!'

# Folder Struktur erstellen
# schauen ob results existiert oder nicht
if [[ ! -d "results" ]]
then 
    echo "Results Struktur wird erstellt."
    mkdir results
fi

mkdir $simulation

# Folder für gespeicherte Modelle
if [[ ! -d "saved_model" ]]
then 
    echo "Ordner für fertig trainierte Modelle erstellt."
    mkdir saved_model
fi

# Alles in einen extra Folder für die Simulation in results stecken.
current_time=$(date "+%F_%H:%M:%S")

cp $dep.txt $simulation/$current_time.$dep.txt
cp $simulation.py $simulation/$current_time.$simulation.py

# Benutzer Eingabe der Quelldatei
# Ausgabe des Zeitpunktes wo die Berechnung durch ist:
echo 'Start der Simulation.'
python3 $simulation.py > log_file.log
cp log_file.log $simulation/$current_time.log_file.log    # log der simulation
cp -R saved_model/ $simulation/$current_time.saved_model  # gespeicherte modelle
cp -R $simulation results
rm log_file.log    # entfernen generic log
# rm $dep.txt $simulation.py
rm -r saved_model  # entfernen generic model folder

# Löschen des .venv
rm -r .$simulation
echo 'Umgebung gelöscht!'

# Fertig!
echo 'Berechnungen beendet!'
