# libraries
import sys
import time
import random
import copy

import pandas as pd
import sklearn as sk
import tensorflow as tf
from tensorflow import keras
import numpy as np
from numpy.linalg import inv
from tensorflow.python.ops.gen_array_ops import scatter_nd_eager_fallback
from tensorflow.python.ops.gen_logging_ops import timestamp

# simulieren von f(x) = x^2
# x als normal(0, 1)

# Daten simulieren: y = beta_0 + error
# Set seed
random.seed(42)

# Daten erstellen;
# X = np.random.normal(loc=0.0, scale=1.0, size =30)
X = np.ones(30)
X = X.reshape(30, 1)
# beta = beta0:1, beta1:3
koef = np.array((3))
koef = koef.reshape(1,1)
# Residuen
resid = np.random.normal(loc=0.0, scale=0.1, size=30)
resid = resid.reshape(30, 1)
# y berechnen mit intercept 1.5
y = X * koef + resid

# Für besseren Umgang mit Tensorflow:
y = pd.DataFrame(y)
X = pd.DataFrame(X)

# Baseline Modell fitten:
# Hyperparameter
## Loss
def my_loss(y_true,y_pred): 
	# loss = -tf.reduce_sum(tf.math.log(f(y_true,y_pred))) # MSE, wir woller aber L2 Norm
  loss = tf.norm((y_true- y_pred), ord='euclidean') # nutzen die L2 Norm nach Tensorflow 2.7
  return loss

## Optimizer 
opt = tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.0, nesterov=False, name='SGD')

## Model
model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(1, use_bias=False, activation='linear', batch_input_shape=(None, 1)))
model0.compile(loss=my_loss, optimizer=opt)
model0.fit(X, y, epochs = 300, use_multiprocessing=True, verbose=1)
# Analyse:
model0.summary()
model0.get_weights()
coefs_m0 = np.vstack(model0.get_weights()[0])

## Forward Diff:
gewichte_m1 = [ ]
h = 0.0001

for i in range(len(X)):
  temp_X = copy.deepcopy(X)
  # temp = temp_X[i]
  temp_X.loc[i] += h
  # print(temp_X)
  np.sum(temp_X - X)
  # Fitten der Modelle:
  model1 = tf.keras.Sequential()
  model1.add(tf.keras.layers.Dense(1, use_bias=False, activation='linear', batch_input_shape=(None, 1)))
  model1.compile(loss=my_loss, optimizer=opt)
  model1.fit(temp_X, y, epochs = 300, use_multiprocessing=True, verbose=0)
  coefs_m1 = np.vstack((model1.get_weights()[0]))
  gewichte_m1.append( coefs_m1 )

np.sum((gewichte_m1 - coefs_m0)/h)

# Mit y:
gewichte_m2 = [ ]

for i in range(len(X)):
  temp_y = copy.deepcopy(y)
  # temp = temp_X[i]
  temp_y.loc[i] += h
  # print(temp_X)
  np.sum(temp_y - y)
  # Fitten der Modelle:
  model2 = tf.keras.Sequential()
  model2.add(tf.keras.layers.Dense(1, use_bias=False, activation='linear', batch_input_shape=(None, 1)))
  model2.compile(loss=my_loss, optimizer=opt)
  model2.fit(X, temp_y, epochs = 300, use_multiprocessing=True, verbose=0)
  coefs_m2 = np.vstack((model2.get_weights()[0]))
  gewichte_m2.append( coefs_m2 )

np.sum((gewichte_m2 - coefs_m0)/h)

# Mit y und mit intercept
gewichte_m3 = [ ]


## Model
model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(1, activation='linear', batch_input_shape=(None, 1)))
model0.compile(loss=my_loss, optimizer=opt)
model0.fit(X, y, epochs = 300, use_multiprocessing=True, verbose=1)
# Analyse:
model0.summary()
model0.get_weights()
coefs_m0 = np.vstack((model0.get_weights()[1], model0.get_weights()[0]))

for i in range(len(X)):
  temp_y = copy.deepcopy(y)
  # temp = temp_X[i]
  temp_y.loc[i] += h
  # print(temp_X)
  np.sum(temp_y - y)
  # Fitten der Modelle:
  model1 = tf.keras.Sequential()
  model1.add(tf.keras.layers.Dense(1, activation='linear', batch_input_shape=(None, 1)))
  model1.compile(loss=my_loss, optimizer=opt)
  model1.fit(X, temp_y, epochs = 300, use_multiprocessing=True, verbose=0)
  coefs_m3 = np.vstack((model1.get_weights()[1], model1.get_weights()[0]))
  gewichte_m3.append( coefs_m3 )

np.sum((gewichte_m3 - coefs_m0)/h)

# Mit x = [x1, x2]
# Daten erstellen;
X = np.random.normal(loc=1.0, scale=1.0, size =60)
# X = np.ones(30)
X = X.reshape(30, 2)
# beta = beta0:1, beta1:3
koef = np.array((2, 3))
koef = koef.reshape(2,1)
# Residuen
resid = np.random.normal(loc=0.0, scale=0.1, size=30)
resid = resid.reshape(30, 1)
# y berechnen mit intercept 1.5
y = np.matmul(X, koef) + resid

# Für besseren Umgang mit Tensorflow:
y = pd.DataFrame(y)
X = pd.DataFrame(X)

# Baseline und Gegenmodel
## beide ohne interncept

## Model
model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(1, use_bias=False, activation='linear', batch_input_shape=(None, 2)))
model0.compile(loss=my_loss, optimizer=opt)
model0.fit(X, y, epochs = 300, use_multiprocessing=True, verbose=1)
# Analyse:
model0.summary()
model0.get_weights()
coefs_m0 = np.vstack((model0.get_weights()[0]))

gewichte_m4 = []

for i in range(len(X)):
  temp_y = copy.deepcopy(y)
  # temp = temp_X[i]
  temp_y.loc[i] += h
  # print(temp_X)
  np.sum(temp_y - y)
  # Fitten der Modelle:
  model1 = tf.keras.Sequential()
  model1.add(tf.keras.layers.Dense(1, use_bias=False, activation='linear', batch_input_shape=(None, 2)))
  model1.compile(loss=my_loss, optimizer=opt)
  model1.fit(X, temp_y, epochs = 300, use_multiprocessing=True, verbose=0)
  coefs_m4 = np.vstack((model1.get_weights()[0]))
  gewichte_m4.append( coefs_m4 )

np.sum((gewichte_m4 - coefs_m0)/h)