# selbe simulierungen wie vorher nur dieses mal mit einer gemischte Aktivierungsfunktion.
# erstelle ein env
# conda env create -f dependencies.yml -n tensorflow
# source .tensorflow 

# install dependencies

# libraries
import sys
import time
import random
import copy 

import pandas as pd
import sklearn as sk
import tensorflow as tf
from tensorflow import keras
import numpy as np
from tensorflow.python.ops.gen_array_ops import scatter_nd_eager_fallback
from tensorflow.python.ops.gen_logging_ops import timestamp


# Test ob alles okay is
print(f"Tensor Flow Version: {tf.__version__}")
print(f"Keras Version: {tf.keras.__version__}")
print()
print(f"Python {sys.version}")
print(f"Pandas {pd.__version__}")
print(f"Scikit-Learn {sk.__version__}")
gpu = len(tf.config.list_physical_devices('GPU'))>0
print("GPU is", "available" if gpu else "NOT AVAILABLE")


# Set seed
random.seed(42)


# Daten erstellen;
X = np.random.normal(loc=0.0, scale=1.0, size =100000)
X = X.reshape(10000, 10)

koef = np.random.uniform(1, 10, 10)
koef = koef.reshape(10, 1)
resid = np.random.normal(loc=0.0, scale=1.0, size=10000)
resid = resid.reshape(10000, 1)
y = 1.5 + np.matmul(X, koef) + resid
y = pd.DataFrame(y)
X = pd.DataFrame(X)

# dataset = pd.DataFrame(dataset, columns=['y', 'x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10'])

# B Matrix
h = 1e-5
# h hierbei nur auf y beziehen.
# Datenset + h
y_ph = y + h
# Datenset - h
y_mh = y - h

# Splitten der Daten
train_dataset = copy.deepcopy(X)
# train_dataset = X.sample(frac=0.8, random_state=0)
test_dataset = copy.deepcopy(train_dataset)

# Split features from labels
train_features = copy.deepcopy(train_dataset)
test_features = copy.deepcopy(test_dataset)

train_labels = y
train_labels_ph = y_ph
train_labels_mh = y_mh
test_labels = y
test_labels_ph = y_ph
test_labels_mh = y_mh


# Hyperparameter

## Loss
def my_loss(y_true,y_pred): 
	# loss = -tf.reduce_sum(tf.math.log(f(y_true,y_pred))) # MSE, wir woller aber L2 Norm
  loss = tf.norm((y_true- y_pred), ord='euclidean') # nutzen die L2 Norm nach Tensorflow 2.7
  return loss

## Optimizer 
opt = tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.0, nesterov=False, name='SGD')

# Modelle
tik = time.time()

# Base Model
model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model0.add(tf.keras.layers.Dense(1, activation='linear'))
model0.compile(loss=my_loss, optimizer=opt)   
model0.fit(train_features, train_labels, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model0.save('saved_model/b10_t1_0')

# forward differences
model1 = tf.keras.Sequential()
model1.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model1.add(tf.keras.layers.Dense(1, activation='linear'))
model1.compile(loss=my_loss, optimizer=opt)
model1.fit(train_features, train_labels_ph, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model1.save('saved_model/b10_t1_ph')

# backward difference
model2 = tf.keras.Sequential()
model2.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model2.add(tf.keras.layers.Dense(1, activation='linear'))
model2.compile(loss=my_loss, optimizer=opt)
model2.fit(train_features, train_labels_mh, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model2.save('saved_model/b10_t1_mh')

# Base Model
model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model0.add(tf.keras.layers.Dense(10, activation='relu'))
model0.add(tf.keras.layers.Dense(1, activation='linear'))
model0.compile(loss=my_loss, optimizer=opt)
model0.fit(train_features, train_labels, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model0.save('saved_model/b10_t2_0')

# forward differences
model1 = tf.keras.Sequential()
model1.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model1.add(tf.keras.layers.Dense(10, activation='relu'))
model1.add(tf.keras.layers.Dense(1, activation='linear'))
model1.compile(loss=my_loss, optimizer=opt)
model1.fit(train_features, train_labels_ph, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model1.save('saved_model/b10_t2_ph')

# backward difference
model2 = tf.keras.Sequential()
model2.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model2.add(tf.keras.layers.Dense(10, activation='relu'))
model2.add(tf.keras.layers.Dense(1, activation='linear'))
model2.compile(loss=my_loss, optimizer=opt)
model2.fit(train_features, train_labels_mh, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model2.save('saved_model/b10_t2_mh')


# Base Model
model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model0.add(tf.keras.layers.Dense(10, activation='relu'))
model0.add(tf.keras.layers.Dense(10, activation='relu'))
model0.add(tf.keras.layers.Dense(1, activation='linear'))
model0.compile(loss=my_loss, optimizer=opt)
model0.fit(train_features, train_labels, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model0.save('saved_model/b10_t3_0')

# forward differences
model1 = tf.keras.Sequential()
model1.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model1.add(tf.keras.layers.Dense(10, activation='relu'))
model1.add(tf.keras.layers.Dense(10, activation='relu'))
model1.add(tf.keras.layers.Dense(1, activation='linear'))
model1.compile(loss=my_loss, optimizer=opt)
model1.fit(train_features, train_labels_ph, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model1.save('saved_model/b10_t3_ph')

# backward difference
model2 = tf.keras.Sequential()
model2.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model2.add(tf.keras.layers.Dense(10, activation='relu'))
model2.add(tf.keras.layers.Dense(10, activation='relu'))
model2.add(tf.keras.layers.Dense(1, activation='linear'))
model2.compile(loss=my_loss, optimizer=opt)
model2.fit(train_features, train_labels_mh, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model2.save('saved_model/b10_t3_mh')


# Base Model
model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model0.add(tf.keras.layers.Dense(10, activation='relu'))
model0.add(tf.keras.layers.Dense(10, activation='relu'))
model0.add(tf.keras.layers.Dense(10, activation='relu'))
model0.add(tf.keras.layers.Dense(1, activation='linear'))
model0.compile(loss=my_loss, optimizer=opt)
model0.fit(train_features, train_labels, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model0.save('saved_model/b10_t4_0')

# forward differences
model1 = tf.keras.Sequential()
model1.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model1.add(tf.keras.layers.Dense(10, activation='relu'))
model1.add(tf.keras.layers.Dense(10, activation='relu'))
model1.add(tf.keras.layers.Dense(10, activation='relu'))
model1.add(tf.keras.layers.Dense(1, activation='linear'))
model1.compile(loss=my_loss, optimizer=opt)
model1.fit(train_features, train_labels_ph, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model1.save('saved_model/b10_t4_ph')

# backward difference
model2 = tf.keras.Sequential()
model2.add(tf.keras.layers.Dense(10, activation='relu', batch_input_shape=(None, 10)))
model2.add(tf.keras.layers.Dense(10, activation='relu'))
model2.add(tf.keras.layers.Dense(10, activation='relu'))
model2.add(tf.keras.layers.Dense(10, activation='relu'))
model2.add(tf.keras.layers.Dense(1, activation='linear'))
model2.compile(loss=my_loss, optimizer=opt)
model2.fit(train_features, train_labels_mh, batch_size=256, epochs = 30, use_multiprocessing=True, verbose=0)
model2.save('saved_model/b10_t4_mh')

tok = time.time()
duration = tok - tik

print(duration)
print('Berechnungen erfolgreich.')