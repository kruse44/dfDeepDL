# erstelle ein env
# conda env create -f dependencies.yml -n tensorflow
# source .tensorflow 

# install dependencies

# libraries
import sys
import time
import random
import copy

import pandas as pd
import sklearn as sk
import tensorflow as tf
from tensorflow import keras
import numpy as np
from tensorflow.python.ops.gen_array_ops import scatter_nd_eager_fallback
from tensorflow.python.ops.gen_logging_ops import timestamp

# Test ob alles okay is
print(f"Tensor Flow Version: {tf.__version__}")
print(f"Keras Version: {tf.keras.__version__}")
print()
print(f"Python {sys.version}")
print(f"Pandas {pd.__version__}")
print(f"Scikit-Learn {sk.__version__}")
gpu = len(tf.config.list_physical_devices('GPU'))>0
print("GPU is", "available" if gpu else "NOT AVAILABLE")

# Set seed
random.seed(42)

# Daten erstellen;
X = np.random.normal(loc=0.0, scale=1.0, size =30)
X = X.reshape(30, 1)
# beta = beta0:1, beta1:3
koef = np.array((3))
koef = koef.reshape(1,1)
# Residuen
resid = np.random.normal(loc=0.0, scale=0.1, size=30)
resid = resid.reshape(30, 1)
# y berechnen mit intercept 1.5
y = 1 + np.matmul(X, koef) + resid

# Für besseren Umgang mit Tensorflow:
y = pd.DataFrame(y)
X = pd.DataFrame(X)

# Differenz anschauen:
h= 0.001

# Einfaches Model:

# Hyperparameter
## Loss
def my_loss(y_true,y_pred): 
	# loss = -tf.reduce_sum(tf.math.log(f(y_true,y_pred))) # MSE, wir woller aber L2 Norm
  loss = tf.norm((y_true- y_pred), ord='euclidean') # nutzen die L2 Norm nach Tensorflow 2.7
  return loss

## Optimizer 
opt = tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.0, nesterov=False, name='SGD')


# Für jedes n einmal + h

model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(1, activation='linear', batch_input_shape=(None, 1)))
model0.compile(loss=my_loss, optimizer=opt)
model0.fit(X, y, epochs = 300, use_multiprocessing=True, verbose=1)
coefs_m0 = np.vstack((model0.get_weights()[1], model0.get_weights()[0]))

gewichte_m1 = [ ]
h = 0.001

for i in range(len(X)):
  temp_X = copy.deepcopy(X)
  # temp = temp_X[i]
  temp_X[i:1] += h
  # Fitten der Modelle:
  model1 = tf.keras.Sequential()
  model1.add(tf.keras.layers.Dense(1, activation='linear', batch_input_shape=(None, 1)))
  model1.compile(loss=my_loss, optimizer=opt)
  model1.fit(temp_X, y, epochs = 300, use_multiprocessing=True, verbose=1)
  coefs_m1 = np.vstack((model1.get_weights()[1], model1.get_weights()[0]))
  gewichte_m1.append( coefs_m1 )
  
# y hat m0
((coefs_m1[0] + coefs_m1[1] * X) - (coefs_m0[0] + coefs_m0[0] * X)) / h
sum((gewichte_m1[0] - (coefs_m0)) / h)



# y hat m1
(coefs_m1[0] + coefs_m1[0] * X)
