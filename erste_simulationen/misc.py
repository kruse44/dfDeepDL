
# Test ob alles okay is
print(f"Tensor Flow Version: {tf.__version__}")
print(f"Keras Version: {tf.keras.__version__}")
print()
print(f"Python {sys.version}")
print(f"Pandas {pd.__version__}")
print(f"Scikit-Learn {sk.__version__}")
gpu = len(tf.config.list_physical_devices('GPU'))>0
print("GPU is", "available" if gpu else "NOT AVAILABLE")

# Daten erstellen;
X = np.random.normal(loc=0.0, scale=1.0, size =90000)
X = X.reshape(10000, 9)
koef = np.random.uniform(1, 10, 9)
koef = koef.reshape(9, 1)
resid = np.random.normal(loc=0.0, scale=0.1, size=10000)
resid = resid.reshape(10000, 1)
y = 1.5 + np.matmul(X, koef) + resid
dataset = np.column_stack((y, X))
dataset = pd.DataFrame(dataset, columns=['y', 'x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9'])

# Splitten der Daten
train_dataset = dataset.sample(frac=0.8, random_state=0)
test_dataset = dataset.drop(train_dataset.index)

# Split features from labels
train_features = train_dataset.copy()
test_features = test_dataset.copy()

train_labels = train_features.pop('y')
test_labels = test_features.pop('y')


# Hyperparameter

## Loss
def my_loss(y_true,y_pred): 
	# loss = -tf.reduce_sum(tf.math.log(f(y_true,y_pred))) # MSE, wir woller aber L2 Norm
  loss = tf.norm((y_true- y_pred), ord='euclidean') # nutzen die L2 Norm nach Tensorflow 2.7
  return loss

## Optimizer 
opt = tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.0, nesterov=False, name='SGD')


# Modelle
tik = time.time()

# OLS
model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(1, activation='linear', batch_input_shape=(None, 9)))
model0.compile(loss=my_loss, optimizer=opt)
model0.fit(train_features, train_labels, batch_size=256, epochs = 100, use_multiprocessing=True)
model.save('saved_model/OLS_0')

# Model mit einder Tiefe von 1
model1 = tf.keras.Sequential()
model1.add(tf.keras.layers.Dense(10, activation='linear', batch_input_shape=(None, 9)))
model1.add(tf.keras.layers.Dense(1, activation='linear'))
model1.compile(loss=my_loss, optimizer=opt)
model1.fit(train_features, train_labels, batch_size=256, epochs = 100, use_multiprocessing=True)
model.save('saved_model/OLS_0')

# Model mit einder Tiefe von 2
model2 = tf.keras.Sequential()
model2.add(tf.keras.layers.Dense(10, activation='linear', batch_input_shape=(None, 9)))
model2.add(tf.keras.layers.Dense(10, activation='linear'))
model2.add(tf.keras.layers.Dense(1, activation='linear'))
model2.compile(loss=my_loss, optimizer=opt)
model2.fit(train_features, train_labels, batch_size=256, epochs = 100, use_multiprocessing=True)
# model.save('saved_model/OLS_0')

# Model mit einder Tiefe von 3
model3 = tf.keras.Sequential()
model3.add(tf.keras.layers.Dense(10, activation='linear', batch_input_shape=(None, 9)))
model3.add(tf.keras.layers.Dense(10, activation='linear'))
model3.add(tf.keras.layers.Dense(10, activation='linear'))
model3.add(tf.keras.layers.Dense(1, activation='linear'))
model3.compile(loss=my_loss, optimizer=opt)
model3.fit(train_features, train_labels, batch_size=256, epochs = 100, use_multiprocessing=True)
# model.save('saved_model/OLS_0')
# Model mit einder Tiefe von 4
model4 = tf.keras.Sequential()
model4.add(tf.keras.layers.Dense(10, activation='linear', batch_input_shape=(None, 9)))
model4.add(tf.keras.layers.Dense(10, activation='linear'))
model4.add(tf.keras.layers.Dense(10, activation='linear'))
model4.add(tf.keras.layers.Dense(10, activation='linear'))
model4.add(tf.keras.layers.Dense(1, activation='linear'))
model4.compile(loss=my_loss, optimizer=opt)
model4.fit(train_features, train_labels, batch_size=256, epochs = 100, use_multiprocessing=True)
model4.save('saved_model/t4_b10_0')

tok = time.time()
duration = tok -tik
