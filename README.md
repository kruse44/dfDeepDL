# Degrees of Freedom und Hessian von Neural Nets

## Ziel(e):
- Wie kann man die Degrees of Freedom eines Neuronales Netzes bestimmen?
- Welche Faktoren spielen eine Rolle? (Tiefe, Breite, Architektur, Aktivierungsfunktionen)
- Bestimmen der zweiten Ableitung der Loss Funktion

## Vorgehensweise:

### Degrees of Freedom
- Für die Analyse werden Neuronale Netze mit verschiedenen Charakteristiker erstellt
    - Tiefe
    - Breite
    - Architektur
    - Aktivierungsfunktionen
- Hierbei bleibt uns erst einmal nur die einfache L2 Norm als Zielgröße
- Mit Hilfe von Finite Differences werden dann die Degrees of Freedom berechnet

### Zweite Ableitung
- hier ist vor allem erst einmal Literatur Arbeit zu machen.
- LeChun hatte da doch einmal ein paar Paper drüber geschrieben ...

_____

# Erster Teil:

## Aufbau Experiment 1:

Die Freiheitsgrade sind im allgeminen als Spur der Headmatrix ($\hat{H}$) oder als folgendes gegeben

$$
    df = \sum_{i} \dfrac{\delta f(\bold{y})}{\delta y_{i}}
$$

Nutzen wir nun den stoachstischen Schätzer für Degrees of Freedom von Non-Linearen Schätzern nach Ramani et al. (2008), dann können wir die df wie folgt neu formulieren

$$
    \sum_{i} \dfrac{\delta f(\bold{y})}{\delta y_{i}} = \lim_{\epsilon \to 0} E_\mathbf{b} \left[ \mathbf{b}^{T} \dfrac{f(\mathbf{y} + \epsilon \mathbf{b}) - f(\mathbf{y})}{\epsilon}  \right] ,
$$

gegeben einem $\mathbf{b}$ als zero mean iid random Vektor und einer Einheits-Varianz.

Wir wenden nun das Prinzip der Finite Differenzes und eine MC Simulation auf die gegeben Daten an, um sie dann zusammen mit der gegebenen Formeln die dfs auszurechnen.

### Stellschrauben
Wichtig ist es hierbei zu schauen inwiefern die oben genannten Charakteristika des DNNs einen Einfluss auf das Model haben bzw. auf deren Degrees of Freedom.
hierbei gehe ich davon aus, dass alles mithilfe des Gradient Descendes berechnet wird.

- Die betrachteten Tiefen werden sein: [10, 20, 40, 50, 100]
- die Breiten werden sein: [0, 1, 2, 3, 4]
- Archtekturen: Fully Connected, tbd.
- Aktivierungsfunktionen: Linear, invert Tangents und Softmax

## Ergebnisse:

____

## Teil 2: Zweite Ableitung der Loss-Funktion

