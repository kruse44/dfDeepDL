# Experiment

## Ziel(e)

- Das Ziel ist es das Paper "Degrees of Freedom in Deep Neural Networks" von Gao & Jojic (2016) zu replizieren.
- Vereinfachung der Herangehensweise mit Finite Differences
- Anwendung beider Methoden auf ein Neuronales Netz in Regressions-Setting
- Damit Abwendung vom Paper, da dort Classification im Zentrum steht

## Experiment Design

Das Experiment besteht aus mehreren Phasen mit explliziten Fragestellungen, welche geklärt werden müssen.

____
## Erste Phase: Machbarkeit

Problemstellung:
- Kann ich das Paper replizieren?
- Geht es auch "nur" mit Finite Differenzes?
- Wie komplex muss ich die Herangehensweise machen?

### Aufbau
- Simulationen mit "Regressions"-Modellen in Tensorflow
- Variieren von Anzahl der Regressoren (beta0, beta0 + beta1, ..., beta0 + beta1 + beta2 + beta3 )
- Erst einmal nur Forward Differenzes machen
- Anschauen wie die Differenz (h) gemacht werden muss (h = 0.1, ..., 0.0001)


### Ergebnisse:
- Kann es ohne Probleme bis DF = 2 replizieren.
- Ab DF = 3 Probleme beim Berechnen der DFs.
- Beim erstes durchlaufen gibt es immer Probleme mit der Initialisierung.

### Take-Away:
- Bei einfachen Modellen geht es ohne Probleme.
- Finite Forward Differenzen und oder die Methode stoßen an ihre Grenzen bei komplexeren Modellen.

____

## Zweite Phase: Optimierung

Problemstellung:
- Kann ich das ganze für komplexere Probleme replizieren? 
- Was spielt hierbei die größte Rolle?
  - Die Differenz (h)?
  - Die Anzahl der Beobachtungen/Iterationen (n)?
  - Die Finite Differenz Methodik?
### Aufbau:
- Das erste Model finden, welches schon oft kleine Probleme gibt.
- Dann n erhöhen, im nächsten Durchlauf h.
- Dann Finite Differenz ändern
  - Forward Diff
  - Centered Diff
  - Weighted Viertel Diff

### Ergebnisse:
- Die Differenz zwischen 0.01 und 0.0001 verbessert leider nicht die Verlässlichkeit der Methodik.
- Der Einfluss auf den Fit ist nicht messbar, in Qualität und in Zeit.
- Die Anzahl (n) verbessert es leider auch nicht.
- Skaliert natürlich nicht linear, da mehr Iterationen bei Finite Differenz und beim fitten. 
### Take-Away:
