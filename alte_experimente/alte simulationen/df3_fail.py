# Simulation ohne Intercpt, Differenz (h) auf y.

# libraries
import sys
import time
import random
import copy

import pandas as pd
import sklearn as sk
import tensorflow as tf
from tensorflow import keras
import numpy as np
from numpy.linalg import inv
from tensorflow.python.ops.gen_array_ops import scatter_nd_eager_fallback
from tensorflow.python.ops.gen_logging_ops import timestamp

# Set seed
random.seed(42)

# Anzahl der Beobachtungen (n):
n = 100

# Mit x = [x1, x2]
# Daten erstellen;
X = np.random.normal(loc=1.0, scale=1.0, size = (n * 2))
# X = np.ones(30)
X = X.reshape(n, 2)
# beta = beta0:1, beta1:3
koef = np.array((3, 5))
koef = koef.reshape(2,1)
# Residuen
resid = np.random.normal(loc=0.0, scale=0.1, size=n)
resid = resid.reshape(n, 1)
# y berechnen mit intercept 1.5
y = 1 + np.matmul(X, koef) + resid
# Für besseren Umgang mit Tensorflow:
y = pd.DataFrame(y)
X = pd.DataFrame(X)


# Hyperparameter
## Loss
def my_loss(y_true,y_pred): 
	# loss = -tf.reduce_sum(tf.math.log(f(y_true,y_pred))) # wir woller aber L2 Norm
  loss = tf.norm((y_true- y_pred), ord='euclidean') # nutzen die L2 Norm nach Tensorflow 2.7
  return loss

## Optimizer 
opt = tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.0, nesterov=False, name='SGD')

# Grundlagen Model: 
## Model
model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(1,  activation='linear', batch_input_shape=(None, 2)))
model0.compile(loss=my_loss, optimizer=opt)
model0.fit(X, y, epochs = 300, use_multiprocessing=True, verbose=0)
# Analyse:
model0.summary()
model0.get_weights()
coefs_m0 = np.vstack(model0.get_weights()[0])
yhat = model0.predict(X)

# Forward Diff:
gewichte_m1 = [ ]
h = 0.001

# Mit y:
y_iter = []
# Gewichte:
gewichte_m2 = [ ]
# i-ter Fit
fits = []

start_sim = time.time()

for i in range(len(X)):
  temp_y = copy.deepcopy(y)
  # temp = temp_X[i]
  temp_y.loc[i] += h
  y_iter.append(temp_y)
  print("Differenz von ", h, " auf y_i :", i)
  np.sum(temp_y - y)
  print("Iteration: ", (i + 1) , " von ", n)
  # Fitten der Modelle:
  start = time.time()
  model2 = tf.keras.Sequential()
  model2.add(tf.keras.layers.Dense(1,  activation='linear', batch_input_shape=(None, 2)))
  model2.compile(loss=my_loss, optimizer=opt)
  model2.fit(X, temp_y, epochs = 300, use_multiprocessing=True, verbose=0)
  end = time.time()
  print("Dauer des Modelfit: ", end - start)
  coefs_m2 = np.vstack((model2.get_weights()[0]))
  gewichte_m2.append( coefs_m2 )
  temp_pred = model2.predict(X)
  fits.append(temp_pred[i])
#   temp_fit = X * coefs_m2
#   fits.append(temp_fit.iloc[i])
end_sim = time.time()
print(end_sim - start_sim)
np.sum((fits - yhat) / h)