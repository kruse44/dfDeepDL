# Importieren der Libraries.
# System libraries
import random
import copy
# Custom libraries
import pandas as pd
import tensorflow as tf
from tensorflow import keras
import numpy as np
from numpy.linalg import inv
import matplotlib.pyplot as plt
# visualisierung
#import tensorboard
#tensorboard.__version__

# random seeds
random.seed(42)
tf.random.set_seed(42)

# Difference:
h = 1e-5

# Daten simulieren:
n = 100
X = np.random.normal(loc=0.0, scale=1.0, size =n*9)
X = X.reshape(n, 9)
koef = np.array((1,2,3,4,5,6,7,8,9))
koef = koef.reshape(9,1)
noise = np.random.normal(loc=0.0, scale=1.0, size=n)
noise = noise.reshape(n, 1)
y = 1 + np.matmul(X, koef) + noise
y = pd.DataFrame(y)
X = pd.DataFrame(X)

# Tensorflow Hyperparamter
opt = tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.0, nesterov=False, name='SGD')

# Modelle
# Jedes Mal Seed enforcen. Sicher ist sicher.
## y = b0 + b1+x1 (model k = 1)
# random seeds
random.seed(42)
tf.random.set_seed(42) 
model1 = tf.keras.Sequential()
model1.add(tf.keras.layers.Dense(1, activation='linear', batch_input_shape=(None, 1)))
model1.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
model1.summary()
hist1 = model1.fit(X.iloc[:, 0], y, epochs = 300, batch_size=100, use_multiprocessing=True, verbose=0)

## y = b0 + b1+x1 (model k = 1)
# random seeds
random.seed(42)
tf.random.set_seed(42) 
model2 = tf.keras.Sequential()
model2.add(tf.keras.layers.Dense(1, activation='linear', batch_input_shape=(None, 2)))
model2.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
model2.summary()
hist2 = model2.fit(X.iloc[:, 0:2], y, epochs = 300, batch_size=100, use_multiprocessing=True, verbose=0)

## y = b0 + b1+x1 + b2+x2 (model k = 2)
# random seeds
random.seed(42)
tf.random.set_seed(42) 
model3 = tf.keras.Sequential()
model3.add(tf.keras.layers.Dense(1, activation='linear', batch_input_shape=(None, 2)))
model3.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
model3.summary()
hist3 = model3.fit(X.iloc[:, 0:3], y, epochs = 300, batch_size=100, use_multiprocessing=True, verbose=0)
