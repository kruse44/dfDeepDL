# Simulation ohne Intercpt, Differenz (h) auf y.

# libraries
import sys
import time
import random
import copy

import pandas as pd
import sklearn as sk
import tensorflow as tf
from tensorflow import keras
import numpy as np
from numpy.linalg import inv
from tensorflow.python.ops.gen_array_ops import scatter_nd_eager_fallback
from tensorflow.python.ops.gen_logging_ops import timestamp

# Set seed
random.seed(42)
tf.random.set_seed(42)

# Model erschaffen bei dem die Finite Forward Differences Probleme haben.
# Bisher immer bei drei Regressoren.
# y = b0 + b1 * x1 + b2 * x2 

# Anzahl der Beobachtungen:
n = 100
# Daten simulieren
X = np.random.uniform(low=1, high=5, size =(n*2))
# X = np.ones(30)
X = X.reshape(n, 2)
# beta = beta0:1, beta1:3, beta2:5, beta3: -2
koef = np.array((3, 5))
koef = koef.reshape(2,1)
# Residuen
resid = np.random.normal(loc=0.0, scale=1, size=n)
resid = resid.reshape(n, 1)
# y berechnen mit intercept 1.5
y = 1 + np.matmul(X, koef) + resid
# Für besseren Umgang mit Tensorflow:
y = pd.DataFrame(y)
X = pd.DataFrame(X)

## Optimizer 
opt = tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.0, nesterov=False, name='SGD')

## Grundlagen Modell:
model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(1, activation='linear', batch_input_shape=(None, 2)))
model0.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
model0.fit(x = X, y = y, batch_size=100, epochs = 100, verbose=2)
# Analyse:
model0.summary()
model0.get_weights()
# coefs_m0 = np.vstack(model0.get_weights()[0])
yhat = model0.predict(X)


# Differences:
h = 0.001
# Forward Differences:
## Objecte zum auslesen erschaffen:
## Liste der jeweils erschaffenen y_i+h:
y_iter_f = []
## Liste der gefitteten Gewichte:
gewichte_f = []
## jeweils iter pred:
preds_f = []
## Liste der gefitteten Modelle
models_f = []

## Forward Models fitten:
for i in range(n):
    print("Iteration: ", (i + 1), " of ", n)
    temp_y = copy.deepcopy(y)
    temp_y.iloc[i] += h
    y_iter_f.append(temp_y)   # die jeweiligen ysen auslesen zu überprüfen ob richtig
    start = time.time()
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Dense(1, activation='linear', batch_input_shape=(None, 2)))
    model.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
    temp_fit = model.fit(x = X, y = temp_y, batch_size = 128, epochs = 1000, verbose = 0)
    end = time.time()
    print("Dauer: ", round((end - start), 4) )
    models_f.append(temp_fit)
    temp_g = model.get_weights()
    gewichte_f.append(temp_g)
    temp_p = model.predict(X) # gesamte predictions, jetzt die i-te auslesen
    preds_f.append(temp_p[i])

preds_f = np.array(preds_f)
fd = np.sum((np.array(preds_f) - yhat)/h)
print('Forward Difference: ', fd)




## Grundlagen Modell:
model0 = tf.keras.Sequential()
model0.add(tf.keras.layers.Dense(20, activation='relu',batch_input_shape=(None, 2)))
model0.add(tf.keras.layers.Dense(1, activation='linear'))
model0.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
model0.fit(x = X, y = y, batch_size=100, epochs = 500, verbose=2)
# Analyse:
model0.summary()
model0.get_weights()
# coefs_m0 = np.vstack(model0.get_weights()[0])
yhat = model0.predict(X)


# Differences:
h = 0.001
# Forward Differences:
## Objecte zum auslesen erschaffen:
## Liste der jeweils erschaffenen y_i+h:
y_iter_f = []
## Liste der gefitteten Gewichte:
gewichte_f = []
## jeweils iter pred:
preds_f = []
## Liste der gefitteten Modelle
models_f = []

## Forward Models fitten:
for i in range(n):
    print("Iteration: ", (i + 1), " of ", n)
    temp_y = copy.deepcopy(y)
    temp_y.iloc[i] += h
    y_iter_f.append(temp_y)   # die jeweiligen ysen auslesen zu überprüfen ob richtig
    start = time.time()
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Dense(20, activation='relu',batch_input_shape=(None, 2)))
    model.add(tf.keras.layers.Dense(1, activation='linear'))
    model.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer=opt)
    temp_fit = model.fit(x = X, y = temp_y, batch_size = 100, epochs = 500, verbose = 0)
    end = time.time()
    print("Dauer: ", round((end - start), 4) )
    models_f.append(temp_fit)
    temp_g = model.get_weights()
    gewichte_f.append(temp_g)
    temp_p = model.predict(X) # gesamte predictions, jetzt die i-te auslesen
    preds_f.append(temp_p[i])

preds_f = np.array(preds_f)
fd2 = np.sum((np.array(preds_f) - yhat)/h)
print('Forward Difference: ', fd2)

print(fd, fd2)