# OLS in python

# Grundsätzliche
import sys
import random
import copy
import statsmodels.api as sm

# tensorflow imports
import pandas as pd
import sklearn as sk
import tensorflow as tf
from tensorflow import keras
import numpy as np
from numpy.linalg import inv
from tensorflow.python.ops.gen_array_ops import scatter_nd_eager_fallback
from tensorflow.python.ops.gen_logging_ops import timestamp

# Zeitmessung
import time

# Set seed
random.seed(42)

# Daten Simulation:
n = 1000
residuals = np.random.normal(loc=0.0, scale=1, size=n)
residuals = residuals.reshape(n, 1)
betas = np.array((1, 2, 3, 4, 5, 6, 7, 8))
betas = betas.reshape(8,1)
x0 = np.ones(n)
x0 = x0.reshape(n, 1)
x = np.random.uniform(low=1, high=2, size =(n*7))
x = x.reshape(n, 7)
X = np.hstack((x0, x))
Xt = X.transpose()
Xb = np.matmul(X, betas)
y = Xb + residuals

# Dataframe simple.
# y = pd.DataFrame(y)
# X = pd.DataFrame(X)

# Basemodel fitten:
XtX = np.matmul(Xt, X)
XtX_inv =  np.linalg.inv(XtX)
Xty = np.matmul(Xt, y)
beta_hat = np.matmul(XtX_inv, Xty)
beta_hat

# model = sm.OLS(y, X).fit()
# print(model.summary())

# Model fit berechnen:
y_hat = np.matmul(X, beta_hat) 

# Schritt 2: auf i-te Zeile h addieren
# y + h und iterativ u. model fitten
h = 0.001

# h auf jedes y_i addieren
# dann model fitten und die fitted values vergleichn

# List aller gefitteten Modelle
model_list = []

for i in range(n):
    temp_y = copy.deepcopy(y)
    temp_y[i] += h
    temp_Xty = np.matmul(Xt, temp_y)
    temp_beta_hat = np.matmul(XtX_inv, temp_Xty)
    temp_fitted = np.matmul(X, temp_beta_hat)
    temp_yhat = temp_fitted[i]
    model_list.append(temp_yhat)

# differenz zwischen eigentlichem und pred model
np.sum((model_list - y_hat)/h)